<!doctype html>
<html>
<head>
    <meta charset="utf-8">

    <title>@yield('title')</title>

    </head>
<body>
<div class="container">

    <header class="row">
        @include('includes.header')
        <link rel="stylesheet" href="/css/app.css" />
    </header>

    <article class="row">

         @yield('content')

    </article>

</div>
</body>
</html>
